const DataController = require('./controller/DataController');
const validatePhone = require('./service/validate.service');
const CampaignController = require('./controller/CampaignController');
const OtpController = require('./controller/OtpController');
const otpUtil = require('./util/otp.util');
const dateService = require('./service/date.service');
const campaignUtil = require('./util/campaign.util');
const BrandNameAds = require('./domain/BrandNameAds.class');
const logger = require('./config/logger');

otpFunction = async function () {
    return new Promise(async function (resolve, reject) {
        campaignUtil.CampaignName += dateService.formatDate(new Date);
        console.log(campaignUtil.CampaignName);
        await checkTableExist();
        const getData = await DataController.getPhoneNumber();
        if(getData.length < 1){
            resolve(false);
            return;
        }
        const getValidPhone = await validatePhone.isPhoneNumber(getData);
        let phoneListCamp = [];
        for (const phone of getValidPhone) {
            if (phone.TYPE_SMS === 0) {
                await updateAfterSend(phone);
            } else {
                phoneListCamp.push(phone);
            }
        }
        await updateAfterSendCampaign(phoneListCamp);
        resolve(true);
    });
};

let updateAfterSendCampaign = async function (phoneListCamp) {
    const sendSMSCamp = await sendCampaign(campaignUtil, phoneListCamp);
    console.log(sendSMSCamp);
    try {
        if (sendSMSCamp) {
            for (const phoneCamp of phoneListCamp) {
                await moveData(0, 1, phoneCamp)
            }
        } else {
            for (const phoneCamp of phoneListCamp) {
                await moveData(1, 1, phoneCamp)
            }
        }
    } catch (err) {
        logger.error(err);
        throw err
    }
};

let updateAfterSend = async function (phone) {
    const result = await sendOTP(otpUtil, phone);
    console.log(result);
    try {
        if (result) {
            console.log('success');
            console.log("---------------------------insert+update+delete-------------------------------")
            await moveData(0,1,phone);
        } else {
            console.log("---------------------------insert+update+delete-------------------------------")
            await moveData(1,1,phone);
        }
    } catch (err) {
        logger.error(err);
        throw err;
    } finally {
        console.log('send otp');
    }
};

let moveData = async function (rslt, status, phone) {
    await DataController.updateRegiterMSG(rslt, status, phone);
    const resultSelect = await DataController.getRow(phone);
    const resultInsert = await DataController.insertMSG(resultSelect[0]);
    console.log("---------------------------insert-------------------------------")
    console.log(resultInsert);
    console.log("---------------------------delete-------------------------------")
    const resultDelete = await DataController.deleteMSG(phone);
    console.log(resultDelete);
};

let sendOTP = async function (otp, phone) {
    const getAuth = await OtpController.getAuth();
    const sendSMS = await OtpController.sendBrandNameOTP(otp, phone, getAuth.access_token);
    if (sendSMS) {
        console.log("---------------------------update success send-------------------------------")
        await DataController.updateRegiterMSG(0, 1, phone);
    } else {
        console.log("---------------------------update failed send-------------------------------")
        await DataController.updateRegiterMSG(1, 1, phone);
    }
    return sendSMS;
};

let checkTableExist = async function () {
    const tableCheck = await DataController.selectTable();
    console.log(tableCheck);
    if (tableCheck.length < 1) {
        const tableCreate = await DataController.createTable();
        console.log(tableCreate);
    } else {
        console.log('nothing to do');
    }
};

let sendCampaign = async function (campaign, phone) {
    const getAuth = await CampaignController.getAuth();
    const createCampaign = await CampaignController.createCampaign(campaign, getAuth.access_token);
    let brandName = new BrandNameAds(getAuth.access_token, 'abcde', createCampaign.CampaignCode, phone);
    const sendCampaign = await CampaignController.sendSMS(brandName);
    console.log(sendCampaign);
    return sendCampaign;
};

module.exports = otpFunction;